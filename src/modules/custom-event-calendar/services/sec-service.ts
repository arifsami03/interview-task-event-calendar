import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { EventInterface, HourInterface } from 'modules/custom-event-calendar/interfaces';

@Injectable()
export class SECService {
    private eventDataUri: String = './assets/data/event.data.json';
    private hoursDataUri: String = './assets/data/hours.data.json';

    constructor(private http: Http) {
    }

    getEventData(): Observable<EventInterface[]> {
        return this.http.get(`${this.eventDataUri}`).pipe(map(data => {
            return <EventInterface[]>data.json();
        }));
    }

    getWorkingHour(): Observable<HourInterface[]> {
        return this.http.get(`${this.hoursDataUri}`).pipe(map(data => {
            return <HourInterface[]>data.json();
        }));
    }

}
