// Import components and services etc here
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule, MatChipsModule } from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';

import { SECService } from 'modules/custom-event-calendar/services';

import { DisplayCalendarComponent } from './components';

export const __IMPORTS = [
    BrowserAnimationsModule,
    FlexLayoutModule,
    MatCardModule,
    MatChipsModule
];

export const __DECLARATIONS = [DisplayCalendarComponent];

export const __PROVIDERS = [SECService];

export const __ENTRY_COMPONENTS = [];
