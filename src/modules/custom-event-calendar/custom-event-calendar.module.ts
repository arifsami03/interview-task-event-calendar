import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';

import { SharedModule } from 'modules/shared/shared.module';

import {
  __IMPORTS,
  __DECLARATIONS,
  __PROVIDERS,
  __ENTRY_COMPONENTS
} from './components.barrel';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    __IMPORTS,
    SharedModule
  ],
  declarations: [__DECLARATIONS],
  providers: [__PROVIDERS],
  entryComponents: [__ENTRY_COMPONENTS],
  exports: [__DECLARATIONS]
})
export class CustomEventCalendarModule { }