export interface EventInterface {

    start: string;
    end: string;
    title: string;

} 