export interface HourInterface {

    hour: number;
    minute: number;
    second: number;

}