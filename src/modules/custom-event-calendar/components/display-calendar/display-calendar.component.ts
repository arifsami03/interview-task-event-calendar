import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

import { PageAnimation } from 'modules/shared/helper';

import { SECService } from 'modules/custom-event-calendar/services';
import { EventInterface, HourInterface } from 'modules/custom-event-calendar/interfaces';

@Component({
  selector: 'display-calendar',
  templateUrl: './display-calendar.component.html',
  styleUrls: ['./display-calendar.component.css'],
  animations: [PageAnimation]
})
export class DisplayCalendarComponent implements OnInit {

  public loaded = false;
  public isError = false;
  public pageState;

  public currentDates = [];
  public workingHours: HourInterface[];
  public eventData: EventInterface[];


  constructor(private secService: SECService) {
  }

  ngOnInit() {

    this.getWorkingHours().then(() => {

      this.getEventData().then(() => {

        this.prepareDates();

      }).catch(error => {

        this.handleError(error);

      });

    }).catch(error => {

        this.handleError(error);
        
      });
  }

  /**
 * Get shop working hours i.e hours from 9AM - 5PM for a day
 * if shopkeeper want to open his shop 9AM - 8PM or 10AM - 3PM then he/she should be able to configure it.
 * 
 */
  private getWorkingHours() {

    return new Promise((resolve, reject) => {

      this.secService.getWorkingHour().subscribe(response => {

        this.workingHours = response;

      }, error => {

        return reject(error);

      }, () => {

        return resolve(true);

      });
    });
  }

  /**
   * Get Event Sample data
   */
  private getEventData() {

    return new Promise((resolve, reject) => {

      this.secService.getEventData().subscribe(response => {

        this.eventData = response;

      }, error => {

        return reject(error);

      }, () => {

        resolve(true);

      });
    });

  }

  private prepareDates() {

    /**
     * Prepare next there days to render and their respecitve data
     */
    for (let i = 1; i < 4; i++) {

      let date = moment().add(i, 'days');

      let currentDate = { date: date.format('DD/MM/YYYY'), dateObj: [] };

      this.workingHours.forEach(item => {

        let datetime = date.set({ 'hour': item.hour, 'minute': item.minute, 'second': item.second }).format();

        let obj = { datetime: datetime, isEvent: false, event: { height: 0 }, title: '' }
        currentDate.dateObj.push(obj);

      });

      this.currentDates.push(currentDate);

    }

    this.checkForEvents();
    this.loaded = true;
    this.pageState = 'active';

  }

  /** check for events */
  checkForEvents() {

    this.eventData.forEach(item => {

      this.currentDates.forEach(currentDate => {

        currentDate.dateObj.forEach(element => {

          if (moment(element.datetime).isSame(item.start)) {

            element.isEvent = true;
            element.title = item.title;

            let hourDiff = moment(item.end).diff(moment(element.datetime), 'hours');

            element.event.height = hourDiff * 50;

          }
        });

      });

    });

  }

  private handleError(error) {

    console.log('error: ', error);
    this.isError = true;
    this.loaded = true;
    this.pageState = 'active';

  }

}
