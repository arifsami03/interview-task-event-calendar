import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { CustomEventCalendarModule } from 'modules/custom-event-calendar/custom-event-calendar.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CustomEventCalendarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
