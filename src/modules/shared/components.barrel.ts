import { MatProgressSpinnerModule } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';

import { PageLoaderComponent } from './components';

export const __IMPORTS = [
  FlexLayoutModule,
  MatProgressSpinnerModule,
];

export const __DECLARATIONS = [
  PageLoaderComponent,
];

export const __PROVIDERS = [];
export const __ENTRY_COMPONENTS = [];
