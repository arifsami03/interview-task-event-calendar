import { trigger, state, style, transition, animate } from "@angular/animations";

/**
 * Animation to load page data.
 */
export const PageAnimation = trigger('pageState', [
    state('*', style({
        transform: 'scale(0.99)',
        opacity: '0',
    })),
    state('active', style({
        transform: 'scale(1)',
        opacity: '1'
    })),
    transition('* => active', animate('300ms ease-in')),
    transition('active => *', animate('250ms ease-in'))
]);
